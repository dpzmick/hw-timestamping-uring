all: main udp udp_send

main: main.c Makefile
	gcc main.c -o main -luring -Wall -Wextra -Werror -g

udp: udp.c Makefile
	gcc udp.c -o udp -Wall -Wextra -Werror -g

udp_send: udp_send.c Makefile
	gcc udp_send.c -o udp_send -Wall -Wextra -Werror -g
