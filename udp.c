#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <liburing.h>
#include <linux/net_tstamp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>

#define GROUP "239.1.1.251"
#define PORT  54100

#define Fail(...) {               \
  fprintf( stderr, __VA_ARGS__);  \
  fprintf( stderr, "\n" );        \
  abort();                        \
  }

int main() {
  int fd = socket( AF_INET, SOCK_DGRAM, 0 );
  if( fd < 0 ) Fail( "Failed to create socket" );

  uint32_t yes = 1;
  setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof( yes ) );

  struct sockaddr_in addr[1];
  memset( addr, 0, sizeof( addr ) );
  addr->sin_family      = AF_INET;
  addr->sin_addr.s_addr = htonl( INADDR_ANY );
  addr->sin_port        = htons( PORT );

  if( bind( fd, (struct sockaddr*)addr, sizeof( addr ) ) < 0 ) {
    Fail( "Failed to bind" );
  }

  struct ip_mreq mreq[1];
  mreq->imr_multiaddr.s_addr = inet_addr( GROUP );
  mreq->imr_interface.s_addr = htonl( INADDR_ANY );

  int ret = setsockopt( fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, mreq, sizeof( mreq ) );
  if( ret < 0 ) Fail( "Failed to join group %s", strerror( errno ) );

  uint32_t optval = SOF_TIMESTAMPING_RX_HARDWARE;
  ret = setsockopt( fd, SOL_SOCKET, SO_TIMESTAMPNS, &optval, sizeof( optval ) );
  if( ret != 0 ) Fail( "failed to setsockopt, errno=%s (%d)", strerror( errno ), errno );

  while( 1 ) {
    char msg_control[40];
    char buf[1500];
    memset( buf, 0, sizeof( buf ) );

    struct iovec iov[1];
    iov->iov_base = buf;
    iov->iov_len  = sizeof( buf );

    struct msghdr hdr[1];
    hdr->msg_iov        = iov;
    hdr->msg_iovlen     = 1;
    hdr->msg_control    = msg_control;
    hdr->msg_controllen = sizeof( msg_control );

    int bytes = recvmsg( fd, hdr, 0 );

    struct cmsghdr* cmsg = CMSG_FIRSTHDR( hdr );
    while( cmsg ) {
      if( cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_TIMESTAMPNS ) {
        struct timespec *	tsp = (void*)CMSG_DATA( cmsg );
        printf( "%zu:", tsp->tv_sec * 100000000 + tsp->tv_nsec );
      }
      cmsg = CMSG_NXTHDR( hdr, cmsg );
    }

    printf( "%d: %s\n", bytes, buf );
  }
}
