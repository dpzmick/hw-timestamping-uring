#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <liburing.h>
#include <linux/net_tstamp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>

// this approach doesn't work at all
// see: https://lore.kernel.org/io-uring/a16d1b24-454e-f96a-4faa-23f68ecaaa6b@kernel.dk/T/#t
// i'd like to get this error myself to confirm

#define Fail(...) {               \
  fprintf( stderr, __VA_ARGS__);  \
  fprintf( stderr, "\n" );        \
  abort();                        \
  }

typedef enum {
  UD_ACCEPT,
  UD_RECV,
} user_data_type_t;

typedef struct {
  user_data_type_t type;
  uint32_t client_idx;
} user_data_t;

static_assert(sizeof(user_data_t) <= 64, "!");

typedef enum {
  ST_UNUSED = 0,
  ST_IO_RING_RESERVED,
  ST_ACTIVE,
} client_state_t;

typedef struct {
  client_state_t     state;

  int                fd;
  struct sockaddr_in addr[1];
  socklen_t          addr_len[1];

  char               buffer[1500];
  char               extra[1500];
  struct iovec       iov[1];
  struct msghdr      msg[1];
} client_t;

typedef struct {
  client_t clients[3];

  struct io_uring ring[1];
  int             sock_fd;
  struct sockaddr_in server_addr[1]; // fixme lifetime?
} server_t;

client_t*
server_find_free_client( server_t*  server,
                         uint32_t * out_idx )
{
  client_t* cl = NULL;
  for( *out_idx = 0; *out_idx < 3; ++out_idx ) {
    if( server->clients[*out_idx].state == ST_UNUSED ) {
      cl = &server->clients[*out_idx];
      break;
    }
  }
  return cl;
}

void
server_post_accept( server_t* server )
{
  uint32_t client_idx;
  client_t* cl = server_find_free_client(server, &client_idx);
  if( !cl ) Fail( "no free clients" );

  struct io_uring_sqe* sqe = io_uring_get_sqe( server->ring );
  if( !sqe ) Fail( "failed to get sqe" );

  io_uring_prep_accept( sqe, server->sock_fd,
                        (struct sockaddr*)cl->addr,
                        cl->addr_len, 0 );
  user_data_t ud;
  ud.type       = UD_ACCEPT;
  ud.client_idx = client_idx;

  void* data = 0;
  memcpy( &data, &ud, sizeof( ud ) );
  io_uring_sqe_set_data( sqe, data );

  printf( "submitting accept for client %d\n", client_idx );
}

void
server_post_recv( server_t*  server,
                  client_t * client,
                  uint32_t   client_idx )
{
  struct io_uring_sqe* sqe = io_uring_get_sqe( server->ring );
  if( !sqe ) Fail( "failed to get sqe" );

  client->iov->iov_base       = client->buffer;
  client->iov->iov_len        = sizeof( client->buffer );
  client->msg->msg_iov        = client->iov;
  client->msg->msg_iovlen     = 1;
  client->msg->msg_control    = client->extra;
  client->msg->msg_controllen = sizeof( client->extra );
  io_uring_prep_recvmsg( sqe, client->fd, client->msg, 0 );

  user_data_t ud;
  ud.type       = UD_RECV;
  ud.client_idx = client_idx;

  void* data = 0;
  memcpy( &data, &ud, sizeof( ud ) );
  io_uring_sqe_set_data( sqe, data );

  printf( "submitting recv for client %d\n", client_idx );
}

server_t*
new_server( void )
{
  int err = 0;
  server_t* server = malloc( sizeof( server_t ) );
  if( !server ) Fail( "failed to allocate" );

  memset( server->clients, 0, sizeof( server->clients ) );
  server->sock_fd = -1;

  err = io_uring_queue_init( 32, server->ring, 0 );
  if( err != 0 ) Fail("failed to init uring");

  server->server_addr->sin_family = AF_INET;
  server->server_addr->sin_addr.s_addr = INADDR_ANY;
  server->server_addr->sin_port = htons( 8888 );

  server->sock_fd = socket( AF_INET, SOCK_STREAM, 0 );
  if( server->sock_fd == -1 ) Fail("failed to socket");

  uint32_t optval = 1;
  setsockopt( server->sock_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof( optval ) );

  optval = SOF_TIMESTAMPING_RX_HARDWARE;
  int ret = setsockopt( server->sock_fd, SOL_SOCKET, SO_TIMESTAMPNS, &optval, sizeof( optval ) );
  if( ret != 0 ) Fail( "failed to setsockopt, errno=%s (%d)", strerror( errno ), errno );

  err = bind( server->sock_fd, (struct sockaddr*)server->server_addr,
              sizeof( server->server_addr ) );

  if( err != 0 ) Fail( "failed to bind" );

  err = listen( server->sock_fd, 0 );
  if( err != 0 ) Fail( "failed to start listening" );

  // add two of them
  server_post_accept( server );
  server_post_accept( server );
  io_uring_submit( server->ring );

  return server;
}

void
server_wait( server_t* server )
{
  struct io_uring_cqe* cqe;
  int err = io_uring_wait_cqe( server->ring, &cqe );
  if( err != 0 ) Fail( "failed to wait" );
  if( !cqe ) Fail( "no cqe" );

  io_uring_cqe_seen( server->ring, cqe );

  user_data_t ud[1];
  memcpy( ud, &cqe->user_data, sizeof( ud ) );

  client_t* client = &server->clients[ ud->client_idx ];

  switch( ud->type ) {
    case UD_ACCEPT: {
      // post another accept to the queue
      server_post_accept( server );

      int fd = cqe->res;
      client->fd = fd;

      printf( "accepting on client %d, got res=%d\n",
              ud->client_idx,
              cqe->res );

      memset( client->buffer, 0, sizeof( client->buffer ) );
      server_post_recv( server, client, ud->client_idx );
      client->state = ST_IO_RING_RESERVED;
      break;
    }
    case UD_RECV: {
      printf( "recv complete on client %d, got res=%d flags=%d\n",
              ud->client_idx,
              cqe->res,
              cqe->flags );

      printf( "msg: %s\n", client->buffer );
      printf( "extra: %s\n", client->extra ); // FIXME
      // FIXME getting einval currently, probably b.c. I change control ptr

      // close socket
      close( client->fd );
      client->state = ST_UNUSED;
      break;
    }
  }

  // fixme don't submit if peek returns that more completions are ready
  // or only submit after handling n reqs
  io_uring_submit( server->ring );
}

int main() {
  server_t* server = new_server();

  while( 1 ) {
    server_wait( server );
  }
}
