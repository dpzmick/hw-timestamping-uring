#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <liburing.h>
#include <linux/net_tstamp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>

#define GROUP "239.1.1.251"
#define PORT  54100

#define Fail(...) {               \
  fprintf( stderr, __VA_ARGS__);  \
  fprintf( stderr, "\n" );        \
  abort();                        \
  }

int main() {
  int fd = socket( AF_INET, SOCK_DGRAM, 0 );
  if( fd < 0 ) Fail( "Failed to create socket" );

  uint32_t yes = 1;
  setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof( yes ) );

  struct sockaddr_in addr[1];
  memset( addr, 0, sizeof( addr ) );
  addr->sin_family      = AF_INET;
  addr->sin_addr.s_addr = inet_addr( GROUP );
  addr->sin_port        = htons( PORT );

  int i = 0;
  while( 1 ) {
    char buf[1500];
    sprintf( buf, "sent message %d", i++ );
    sendto( fd, buf, strlen( buf ), 0, (struct sockaddr*)addr, sizeof( addr ) );
  }
}
